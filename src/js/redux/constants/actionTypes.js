import { actions, Actions, Docs } from '../../util/reduxActionTypes'

function formActions(action) {
  return {
    FAILED: `${action} failed`,
    SUBMIT: `${action} initiated`,
    SUCCESS: `${action} succeeded`
  }
}

export const MEMBERS_CREATED = 'MEMBERS_CREATED'
export const COMMITTEES = actions('COMMITTEES', {
  CREATE: formActions('Create new meeting'),
  UPDATE_LIST: 'The list of committees has been updated',
  FETCH_COMMITTEE: '',
  REQUESTING_COMMITTEE_ACCESS: 'REQUESTING_COMMITTEE_ACCESS'
})
export const MEMBERS_FETCHED = 'MEMBERS_FETCHED'
export const IMPORT_MEMBERS = formActions('Member import')
export const FETCH_EMAIL_TEMPLATES_SUCCEEDED = 'FETCH_EMAIL_TEMPLATES_SUCCEEDED'
export const FETCH_EMAIL_TEMPLATE_SUCCEEDED = 'FETCH_EMAIL_TEMPLATE_SUCCEEDED'
export const SAVE_EMAIL_TEMPLATE = 'SAVE_EMAIL_TEMPLATE'
export const SAVE_EMAIL_TEMPLATE_SUCCEEDED = 'SAVE_EMAIL_TEMPLATE_SUCCEEDED'
export const SAVE_EMAIL_TEMPLATE_FAILED = 'SAVE_EMAIL_TEMPLATE_FAILED'
export const MEETINGS = actions('MEETINGS', {
  CODE: formActions('Update meeting code'),
  CREATE: formActions('New meeting creation'),
  EDIT: 'Edit a meeting',
  UPDATE_LIST: 'Update the list of meetings',
  SET_MEETING_CODE: 'Set the 4 digit meeting code'
})
export const ELECTIONS = actions('ELECTIONS', {
  FETCH_ELECTIONS: 'Fetch all elections',
  FETCH_ELECTION: 'Fetch an election'
})

export const ROOT = Actions({
  EMAIL: {
    // move to RULES?
    FORWARD: {
      ADD: 'Add a forwarding address to an email forwarding rule',
      REMOVE: 'Remove a forwarding address from an email forwarding rule',
      UPDATE: 'Update a forwarding address on an email forwarding rule'
    },
    RULES: {
      CREATE: 'Create a new email forwarding rule',
      DELETE: formActions('Delete email forwarding rule'),
      SAVE: formActions('Save an email forwarding rule'),
      UPDATE_ADDRESS: 'Update the forwarding rule email address',
      UPDATE_LIST: 'The list of email rules has been updated'
    }
  }
})
export const FETCH_EMAIL_TOPICS = 'FETCH_EMAIL_TOPICS'
export const FETCH_EMAIL_ADDRESSES = 'FETCH_EMAIL_ADDRESSES'
export const SEND_EMAIL_TEMPLATE_SUCCEEDED = 'SEND_EMAIL_TEMPLATE_SUCCEEDED'
export const SEND_EMAIL_TEMPLATE_FAILED = 'SEND_EMAIL_TEMPLATE_FAILED'

export const docs = Docs(ROOT)
export const { EMAIL } = ROOT

export const MEMBER_SEARCH_INPUT_UPDATED = 'MEMBER_SEARCH_INPUT_UPDATED'
export const MEMBER_SEARCH_INPUT_CLEARED = 'MEMBER_SEARCH_INPUT_CLEARED'
export const MEMBER_SEARCH_QUERY_REQUESTED = 'MEMBER_SEARCH_QUERY_REQUESTED'
export const MEMBER_SEARCH_QUERY_SUCCEEDED = 'MEMBER_SEARCH_QUERY_SUCCEEDED'
export const MEMBER_SEARCH_QUERY_FAILED = 'MEMBER_SEARCH_QUERY_FAILED'
export const MEMBER_SEARCH_LOAD_MORE_REQUESTED =
  'MEMBER_SEARCH_LOAD_MORE_REQUESTED'
export const MEMBER_SEARCH_LOAD_MORE_SUCCEEDED =
  'MEMBER_SEARCH_LOAD_MORE_SUCCEEDED'
