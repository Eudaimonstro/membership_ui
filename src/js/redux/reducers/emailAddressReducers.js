import { fromJS } from 'immutable'
import * as types from './../constants/actionTypes'

const INITIAL_STATE = fromJS({
  byId: {}
})

export function emailAddresses(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_EMAIL_ADDRESSES:
      return state.set(
        'byId',
        action.payload
          .toMap()
          .mapEntries(([_, address]) => [address.get('id'), address])
      )
    default:
      return state
  }
}

export default emailAddresses
