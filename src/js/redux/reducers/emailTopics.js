import { fromJS } from 'immutable'
import { showNotification } from '../../util/util'
import * as types from './../constants/actionTypes'

const INITIAL_STATE = fromJS({
  byId: {}
})

export function topics(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_EMAIL_TOPICS:
      return state.set(
        'byId',
        action.payload
          .toMap()
          .mapEntries(([_, topic]) => [topic.get('id'), topic])
      )
    case types.SEND_EMAIL_TEMPLATE_SUCCEEDED:
      return state
    case types.SEND_EMAIL_TEMPLATE_FAILED:
      return state
    default:
      return state
  }
}

export default topics
