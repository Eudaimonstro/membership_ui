import { MEMBERS_CREATED, MEMBERS_FETCHED } from '../constants/actionTypes'
import { List, Map } from 'immutable'

export const INITIAL_STATE = Map()

function members(state = INITIAL_STATE, action) {
  switch (action.type) {
    case MEMBERS_CREATED:
      return updateMembersMap(state, List.of(action.payload.get('member')))
    case MEMBERS_FETCHED:
      return updateMembersMap(state, action.payload)
    default:
      return state
  }
}

export default members

function updateMembersMap(state, members) {
  const membersMap = members.toMap().mapEntries(([_, m]) => [m.get('id'), m])
  return state.mergeDeep(membersMap)
}
