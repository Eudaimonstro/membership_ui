import { MEETINGS } from '../constants/actionTypes'
import { fromJS, List } from 'immutable'

const INITIAL_STATE = fromJS({
  byId: {},
  form: {
    code: {
      inSubmission: false
    },
    create: {
      inSubmission: false
    }
  }
})

function meetings(state = INITIAL_STATE, action) {
  switch (action.type) {
    case MEETINGS.CREATE.SUBMIT:
      return state.setIn(['form', 'create', 'inSubmission'], true)
    case MEETINGS.CREATE.SUCCESS:
      return updateMeetingsById(state, List.of(action.payload)).setIn(
        ['form', 'create'],
        INITIAL_STATE.getIn(['form', 'create'])
      )
    case MEETINGS.CREATE.FAILED:
      return state.setIn(['form', 'create', 'inSubmission'], false)
    case MEETINGS.UPDATE_LIST:
      return updateMeetingsById(state, action.payload)
    case MEETINGS.CODE.SUBMIT:
      return state.setIn(['form', 'code', 'inSubmission'], true)
    case MEETINGS.CODE.SUCCESS:
      return setMeetingCode(
        state,
        action.payload.meetingId,
        action.payload.code
      )
    case MEETINGS.CODE.FAILED:
      return state.setIn(['form', 'code', 'inSubmission'], false)
    default:
      return state
  }
}

function updateMeetingsById(state, meetings) {
  const meetingsWithCodeClaimed = meetings
    .toMap()
    .mapEntries(([_, m]) => [m.get('id'), m])
  return state.mergeDeepIn(['byId'], meetingsWithCodeClaimed)
}

function setMeetingCode(state, meetingId, code) {
  const currentMeeting = state.getIn(['byId', meetingId]).set('code', code)
  return updateMeetingsById(state, List.of(currentMeeting))
}

export default meetings
