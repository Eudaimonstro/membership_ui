import { List, fromJS } from 'immutable'
import {
  MEMBER_SEARCH_INPUT_UPDATED,
  MEMBER_SEARCH_INPUT_CLEARED,
  MEMBER_SEARCH_QUERY_REQUESTED,
  MEMBER_SEARCH_QUERY_SUCCEEDED,
  MEMBER_SEARCH_LOAD_MORE_REQUESTED,
  MEMBER_SEARCH_LOAD_MORE_SUCCEEDED
} from '../constants/actionTypes'

export default function(
  state = fromJS({
    query: '',
    results: [],
    hasMoreResults: false,
    isLoading: false,
    cursor: 0
  }),
  action
) {
  switch (action.type) {
    case MEMBER_SEARCH_INPUT_UPDATED:
      if (action.payload === '') {
        return state.merge({
          query: '',
          results: List(),
          hasMoreResults: false
        })
      }
      return state.set('query', action.payload)
    case MEMBER_SEARCH_INPUT_CLEARED:
      return state.merge({
        query: '',
        results: List(),
        hasMoreResults: false
      })
    case MEMBER_SEARCH_QUERY_REQUESTED:
      return state.set('isLoading', true)
    case MEMBER_SEARCH_LOAD_MORE_REQUESTED:
      return state.set('isLoading', true)
    case MEMBER_SEARCH_QUERY_SUCCEEDED:
      return state.merge({
        isLoading: false,
        hasMoreResults: action.payload.get('has_more'),
        results: action.payload.get('members'),
        cursor: action.payload.get('cursor')
      })
    case MEMBER_SEARCH_LOAD_MORE_SUCCEEDED:
      return state.merge({
        isLoading: false,
        hasMoreResults: action.payload.get('has_more'),
        results: state.get('results').concat(action.payload.get('members')),
        cursor: action.payload.get('cursor')
      })
    default:
      return state
  }
}
