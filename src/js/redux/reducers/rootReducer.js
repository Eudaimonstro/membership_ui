import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'

import auth from './auth'
import { Api } from '../../client/ApiClient'
import committees from './committeeReducers'
import elections from './electionReducers'
import member from './memberReducers'
import members from './members'
import memberImport from './memberImportReducers'
import meetings from './meetingReducers'
import templates from './emailTemplates'
import topics from './emailTopics'
import emailAddresses from './emailAddressReducers'
import emails from './emailRuleReducers'
import universalAuth from './universalAuthReducer'
import callback from './callback'
import memberSearch from './memberSearchReducer'

const rootReducer = combineReducers({
  universalAuth,
  callback,
  auth,
  client: () => Api,
  committees,
  elections,
  emails,
  templates,
  member,
  members,
  memberImport,
  meetings,
  topics,
  emailAddresses,
  memberSearch,
  routing
})

export default rootReducer
