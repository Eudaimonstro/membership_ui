import { fromJS, Map } from 'immutable'
import { COMMITTEES } from '../constants/actionTypes'
import { showNotification } from '../../util/util'

const INITIAL_STATE = fromJS({
  byId: {},
  form: {
    create: {
      name: '',
      inSubmission: false
    }
  }
})

function committees(state = INITIAL_STATE, action) {
  switch (action.type) {
    case COMMITTEES.UPDATE_LIST:
      return addCommitteesById(state, action.payload)
    case COMMITTEES.FETCH_COMMITTEE:
      return state.setIn(['byId', action.payload.get('id')], action.payload)
    case COMMITTEES.CREATE.SUBMIT:
      return state.setIn(['form', 'create', 'inSubmission'], true)
    case COMMITTEES.CREATE.SUCCESS:
      const committeeId = action.payload.get('id')
      if (!committeeId) {
        throw new Error(`Missing committee.id from ${action.payload.toJS()}`)
      }
      return addCommitteesById(
        state,
        Map({
          [committeeId]: action.payload
        })
      ).setIn(['form', 'create', 'inSubmission'], false)
    case COMMITTEES.CREATE.FAILED:
      return state.setIn(['form', 'create', 'inSubmission'], false)
    case COMMITTEES.REQUESTING_COMMITTEE_ACCESS:
      showNotification('Sent!', 'Committee request sent.')
    default:
      return state
  }
}

function addCommitteesById(state, committees) {
  const meetingsWithCodeClaimed = committees
    .toMap()
    .mapEntries(([_, m]) => [m.get('id'), m])
  return state.mergeDeepIn(['byId'], meetingsWithCodeClaimed)
}

export default committees
