import { MEETINGS } from '../constants/actionTypes'
import { Meetings } from '../../client/MeetingClient'
import { logError } from '../../util/util'

export const MeetingCodeActions = Object.freeze({
  AUTOGENERATE: 'autogenerate',
  REMOVE: 'remote',
  SET: 'set'
})

export function fetchAllMeetings() {
  return async dispatch => {
    try {
      const meetings = await Meetings.all()
      dispatch({
        type: MEETINGS.UPDATE_LIST,
        payload: meetings
      })
    } catch (err) {
      logError(JSON.stringify(err))
    }
  }
}

export function createMeeting(meeting) {
  return async dispatch => {
    dispatch({
      type: MEETINGS.CREATE.SUBMIT,
      payload: meeting
    })
    try {
      const result = await Meetings.create(meeting)
      const created = result.get('meeting')
      dispatch({
        type: MEETINGS.CREATE.SUCCESS,
        payload: created
      })
    } catch (error) {
      dispatch({
        type: MEETINGS.CREATE.FAILED,
        error,
        payload: meeting
      })
    }
  }
}

export function claimMeetingCode(meetingId, code) {
  return async dispatch => {
    const payload = {
      meetingId,
      code
    }
    dispatch({
      type: MEETINGS.CODE.SUBMIT,
      payload
    })
    const promise =
      code === MeetingCodeActions.AUTOGENERATE
        ? Meetings.autogenerateMeetingCode(meetingId)
        : Meetings.setMeetingCode(meetingId, code)
    try {
      const result = await promise
      dispatch({
        type: MEETINGS.CODE.SUCCESS,
        payload: {
          meetingId,
          code: result.getIn(['meeting', 'code'])
        }
      })
    } catch (error) {
      dispatch({
        type: MEETINGS.CODE.FAILED,
        error,
        payload
      })
    }
  }
}
