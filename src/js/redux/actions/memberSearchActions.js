import { logError } from '../../util/util'
import { Members } from '../../client/MemberClient'
import {
  MEMBER_SEARCH_INPUT_UPDATED,
  MEMBER_SEARCH_INPUT_CLEARED,
  MEMBER_SEARCH_QUERY_REQUESTED,
  MEMBER_SEARCH_QUERY_SUCCEEDED,
  MEMBER_SEARCH_LOAD_MORE_REQUESTED,
  MEMBER_SEARCH_LOAD_MORE_SUCCEEDED
} from '../constants/actionTypes'

const RESULTS_PER_PAGE = 25

export function updateMemberSearchInput(query) {
  return {
    type: MEMBER_SEARCH_INPUT_UPDATED,
    payload: query
  }
}

export function clearMemberSearchInput() {
  return {
    type: MEMBER_SEARCH_INPUT_CLEARED
  }
}

function requestMembersQuery() {
  return {
    type: MEMBER_SEARCH_QUERY_REQUESTED
  }
}

function receiveMembersQuery(payload) {
  return {
    type: MEMBER_SEARCH_QUERY_SUCCEEDED,
    payload
  }
}

export function searchMembers(query) {
  return dispatch => {
    dispatch(requestMembersQuery())
    return Members.search(query, RESULTS_PER_PAGE)
      .then(response => dispatch(receiveMembersQuery(response)))
      .catch(err => logError(err.toString(), err))
  }
}

function requestMoreResults() {
  return {
    type: MEMBER_SEARCH_LOAD_MORE_REQUESTED
  }
}

function receiveMoreResults(payload) {
  return {
    type: MEMBER_SEARCH_LOAD_MORE_SUCCEEDED,
    payload
  }
}

export function loadMoreResults(query, cursor) {
  return dispatch => {
    dispatch(requestMoreResults())
    return Members.search(query, RESULTS_PER_PAGE, cursor)
      .then(response => dispatch(receiveMoreResults(response)))
      .catch(err => logError(err.toString(), err))
  }
}

export default {
  updateMemberSearchInput,
  clearMemberSearchInput,
  searchMembers,
  loadMoreResults
}
