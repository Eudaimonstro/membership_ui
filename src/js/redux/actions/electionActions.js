import { logError } from '../../util/util'
import { Elections } from '../../client/ElectionClient'
import { ELECTIONS } from '../constants/actionTypes'

export function fetchElections() {
  return dispatch => {
    Elections.getElections()
      .then(data => {
        dispatch({
          type: ELECTIONS.FETCH_ELECTIONS,
          payload: data
        })
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}

export function fetchElection(electionId) {
  return dispatch => {
    Elections.getElection(electionId)
      .then(data => {
        dispatch({
          type: ELECTIONS.FETCH_ELECTION,
          payload: data.set('id', electionId)
        })
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}
