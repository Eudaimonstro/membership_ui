import * as types from './../constants/actionTypes'
import { EmailAddresses } from '../../client/EmailAddressClient'

export function fetchEmailAddresses() {
  return async dispatch => {
    const addresses = await EmailAddresses.all()
    dispatch({
      type: types.FETCH_EMAIL_ADDRESSES,
      payload: addresses
    })
  }
}
