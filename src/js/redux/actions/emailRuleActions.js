import { EMAIL } from './../constants/actionTypes'
import { EmailRules } from '../../client/EmailRuleClient'

export function fetchEmailRules() {
  return async dispatch => {
    const emails = await EmailRules.all()
    dispatch({
      type: EMAIL.RULES.UPDATE_LIST,
      payload: emails
    })
  }
}

export function saveEmailRule(email) {
  return async dispatch => {
    let response
    let newRecord = email.get('status', null) === 'new'
    if (newRecord) {
      response = await EmailRules.create(email)
    } else {
      response = await EmailRules.update(email)
    }

    dispatch({
      type: EMAIL.RULES.SAVE.SUCCESS,
      email: response.get('email'),
      new_record: newRecord
    })
  }
}

export function newEmailRule() {
  return dispatch => dispatch({ type: EMAIL.RULES.CREATE })
}

export function updateEmailAddress(emailId, value) {
  return dispatch =>
    dispatch({
      type: EMAIL.RULES.UPDATE_ADDRESS,
      id: emailId,
      email_address: value
    })
}

export function deleteEmailRule(email) {
  return async dispatch => {
    if (email.get('status', null) !== 'new') {
      await EmailRules.delete(email)
    }

    dispatch({
      type: EMAIL.RULES.DELETE.SUCCESS,
      id: email.get('id', 'new')
    })
  }
}

export function addForwardingAddress(emailId) {
  return dispatch =>
    dispatch({
      type: EMAIL.FORWARD.ADD,
      id: emailId
    })
}

export function deleteForwardingAddress(emailId, forwardingIndex) {
  return dispatch =>
    dispatch({
      type: EMAIL.FORWARD.REMOVE,
      id: emailId,
      forwardingIndex: forwardingIndex
    })
}

export function updateForwardingAddress(emailId, forwardingIndex, address) {
  return dispatch =>
    dispatch({
      type: EMAIL.FORWARD.UPDATE,
      id: emailId,
      forwardingIndex: forwardingIndex,
      forwardingAddress: address
    })
}
