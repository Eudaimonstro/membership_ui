import {
  AUTH_IN_PROGRESS,
  UNAUTH_USER,
  AUTH_ERROR,
  CLEARDOWN
} from './authTypes'

import Auth from './universalAuth'

const auth = new Auth()

export function signinUser(redirectPath = null) {
  return function(dispatch) {
    dispatch({ type: AUTH_IN_PROGRESS })
    auth.signin(redirectPath)
  }
}

export function authError(error) {
  const timestamp = Date.now()
  return {
    type: AUTH_ERROR,
    error,
    timestamp
  }
}

export function signoutUser() {
  auth.signout()
  return { type: UNAUTH_USER }
}

export function cleardown() {
  return {
    type: CLEARDOWN
  }
}
