import auth0 from 'auth0-js'
import nanoid from 'nanoid'

import { AUTH0_CLIENT_ID, AUTH0_DOMAIN } from '../../../config'

export default class Auth {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      domain: AUTH0_DOMAIN,
      clientID: AUTH0_CLIENT_ID,
      // audience: `https://${params.domain}/userinfo`,
      redirectUri: `${window.location.origin}/process`,
      scope: 'openid email authorization',
      responseType: 'token'
    })
    this.signin = this.signin.bind(this)
    this.signout = this.signout.bind(this)
    this.handleAuthentication = this.handleAuthentication.bind(this)
    this.isAuthenticated = this.isAuthenticated.bind(this)
  }

  signin(redirectPath = null) {
    const options = {}
    if (redirectPath) {
      // Generate a `nonce` per Auth0's docs
      // https://auth0.com/docs/protocols/oauth2/redirect-users
      const nonce = nanoid()
      window.localStorage.setItem(nonce, JSON.stringify({ redirectPath }))
      options.state = nonce
    }
    this.auth0.authorize(options)
  }

  signout() {
    // Clear access token and ID token from local storage
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    localStorage.removeItem('id_token')
    localStorage.removeItem('expires_at')
  }

  handleAuthentication() {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult)
          this.sendSessionStartEvent()

          return resolve(authResult)
        } else if (err) {
          return reject(err)
        }
      })
    })
  }

  setSession(authResult) {
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    )
    localStorage.setItem('user', JSON.stringify(authResult.idTokenPayload))
    localStorage.setItem('access_token', authResult.accessToken)
    localStorage.setItem('id_token', authResult.idToken)
    localStorage.setItem('expires_at', expiresAt)
  }

  sendSessionStartEvent() {
    document.dispatchEvent(new Event('sessionStarted'))
  }

  isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    let expiresAt = JSON.parse(localStorage.getItem('expires_at'))
    return new Date().getTime() < expiresAt
  }
}
