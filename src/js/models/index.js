import { is } from 'immutable'

export function ImmutableModel(proxy) {
  this._eq = proxy
}

/**
 * Used by Immutable.is for checking equality within Sets and other collections.
 * @param other the other element
 * @returns {boolean} true if the equivalent data containers are equal
 */
ImmutableModel.prototype.equals = function(other) {
  return other instanceof ImmutableModel && is(this._eq, other._eq)
}
