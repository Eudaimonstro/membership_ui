import { Api } from '../client/ApiClient'
import { List } from 'immutable'

export class EmailAddressClient {
  constructor(api) {
    this.api = api
  }

  all() {
    return this.api
      .url('/emails')
      .get()
      .executeOr(List())
  }
}

export const EmailAddresses = new EmailAddressClient(Api)
