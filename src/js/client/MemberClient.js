import { Api } from '../client/ApiClient'
import { Map } from 'immutable'

export class MemberClient {
  constructor(api) {
    this.api = api
  }

  getCurrentUser() {
    return this.api
      .url(`/member/details`)
      .get()
      .executeOr(Map())
  }

  all() {
    return this.api
      .url(`/member/list`)
      .get()
      .execute()
  }

  importRoster(file) {
    return this.api
      .url(`/import`)
      .put()
      .attach('file', file)
      .execute()
  }

  create(member) {
    return this.api
      .url(`/member`)
      .post(member)
      .execute()
  }

  update(attributes) {
    return this.api
      .url(`/member`)
      .put(attributes)
      .execute()
  }

  search(query, pageSize = 10, cursor = 0) {
    return this.api
      .url(`/member/search`)
      .query({
        query: query,
        page_size: pageSize,
        cursor
      })
      .get()
      .execute()
  }

  addRole(memberId, role, committeeId) {
    return this.api
      .url(`/member/role`)
      .post({ member_id: memberId, role: role, committee_id: committeeId })
      .execute()
  }

  removeRole(memberId, role, committeeId) {
    return this.api
      .url(`/member/role`)
      .copy({
        body: { member_id: memberId, role: role, committee_id: committeeId }
      })
      .remove()
      .execute()
  }
}

export const Members = new MemberClient(Api)
