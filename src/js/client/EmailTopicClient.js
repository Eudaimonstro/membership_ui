import { Api } from '../client/ApiClient'
import { List } from 'immutable'

export class EmailTopicClient {
  constructor(api) {
    this.api = api
  }

  all() {
    return this.api
      .url('/email_topics')
      .get()
      .executeOr(List())
  }

  send(topicId, templateId, address) {
    return this.api
      .url('/email_topics/send')
      .post({
        topic_id: topicId,
        template_id: templateId,
        sending_address: address
      })
      .execute()
  }
}

export const EmailTopics = new EmailTopicClient(Api)
