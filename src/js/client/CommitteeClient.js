import { Api } from '../client/ApiClient'
import { Map } from 'immutable'

export class CommitteeClient {
  constructor(api) {
    this.api = api
  }

  async all() {
    const result = await this.api
      .url(`/committee/list`)
      .get()
      .execute()

    return Map(result.map(committee => [committee.get('id'), committee]))
  }

  get(committeeId) {
    return this.api
      .url(`/committee/${committeeId}`)
      .get()
      .execute()
  }

  create({ name, admin_list }) {
    return this.api
      .url(`/committee`)
      .post({
        admin_list: admin_list,
        name: name
      })
      .execute()
  }

  requestMembership(committeeId) {
    return this.api
      .url(`/committee/${committeeId}/member_request`)
      .post()
      .execute()
  }
}

export const Committees = new CommitteeClient(Api)

export default Committees
