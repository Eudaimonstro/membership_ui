import { Api } from './ApiClient'
import { logError } from '../util/util'
import { fromJS } from 'immutable'

export class ElectionClient {
  constructor(api) {
    this.api = api
  }

  submitVote(params) {
    return this.api
      .url(`/vote`)
      .post(params)
      .execute()
  }

  getVote(electionId, ballotKey) {
    return this.api
      .url(`/election/${electionId}/vote/${ballotKey}`)
      .get()
      .composeErrorHandler((handler, res) => {
        return res.error.status === 404 || handler(res)
      })
      .executeOr(null)
  }

  getEligibleVoters(electionId) {
    return this.api
      .url(`/election/eligible/list`, { params: { election_id: electionId } })
      .get()
      .execute()
  }

  getElections() {
    return this.api
      .url('/election/list')
      .get()
      .execute()
  }

  async getElection(id) {
    let result = await this.api
      .url(`/election`, { params: { id } })
      .get()
      .executeOr(null)
    if (result) {
      // add the id since it isn't returned from the db
      result = result.set({ id })
    }
    return result
  }

  countResults(electionId) {
    return this.api
      .url(`/election/count`, {
        params: {
          id: electionId,
          visualization: true
        }
      })
      .get()
      .execute()
  }

  submitPaperBallot(ballot, override = false) {
    const ballotPost = fromJS(ballot).set('override', override)
    return this.api
      .url(`/vote/paper`)
      .composeErrorHandler((handler, { error }) => {
        if (error.status === 404) {
          logError(`Cannot submit unclaimed ballot #${ballot.ballot_key}`)
        } else {
          logError(
            `Unexpected error when submitting ballot ${ballotPost}`,
            error
          )
        }
      })
      .post(ballotPost)
      .execute()
  }

  submitTransition(electionId, transition) {
    return this.api
      .url(`/election/${electionId}/state/${transition}`)
      .put()
      .execute()
  }

  updateElection(electionId, attributes) {
    if (!electionId) {
      throw new Error(`Invalid election_id: ${electionId}`)
    }
    return this.api
      .url(`/election/${electionId}`)
      .patch(attributes.set('election_id', electionId))
      .execute()
  }
}

export const Elections = new ElectionClient(Api)
