import _ from 'lodash'

export class Fn {
  static id(self) {
    return self
  }

  /**
   * Returns the value if the value is null, otherwise returns result of function.
   *
   * @returns either null or the result of fn(value)
   */
  static nullOr(fn, value) {
    return value == null ? value : fn(value)
  }

  /**
   * Returns the value if the value is falsy, otherwise returns result of function.
   *
   * @returns either value (if falsy) or the result of fn(value)
   */
  static falsyOr(fn, value) {
    return !value ? value : fn(value)
  }
}

export class Sanitize {
  static string(fn = Fn.id) {
    return new Sanitize(fn)
  }

  static pipeline(...fns) {
    let combined = new Sanitize(Fn.id)
    fns.forEach(fn => {
      combined = combined.andThen(fn)
    })
    return combined
  }

  static toFn(o) {
    return o.sanitize ? o.sanitize : _.isFunction(o) ? o : null
  }

  constructor(fn) {
    this.sanitize = function(str) {
      return str === null ? null : fn(str)
    }
  }

  compose(prev) {
    return new Sanitize(str => this.sanitize(Sanitize.toFn(prev)(str)))
  }

  andThen(next) {
    return new Sanitize(str => Sanitize.toFn(next)(this.sanitize(str)))
  }
}

const validatePositiveNumExp = /^[1-9]?[0-9]*$/

Sanitize.trimmed = Sanitize.string(s => s.trim())
Sanitize.postiveNum = Sanitize.string(
  str => (validatePositiveNumExp.test(str) ? str : null)
)
