import { Map } from 'immutable'

export function pickObjectOrThrow(map, keys, description) {
  const picked = {}
  const missing = []
  const obj = Map.isMap(map) ? map.toObject() : map
  keys.forEach(key => {
    const found = obj[key]
    if (found === undefined) {
      missing.push(key)
    } else {
      picked[key] = found
    }
  })
  if (missing.length > 0) {
    const debug =
      (description === undefined &&
        (description instanceof Function
          ? description(missing)
          : description)) ||
      JSON.stringify(map)
    throw new Error(`Missing keys '${missing.join("', '")}' from ${debug}`)
  } else {
    return picked
  }
}

export function pickArrayOrThrow(map, keys, description) {
  const obj = pickObjectOrThrow(map, keys, description)
  return keys.map(key => obj[key])
}
