import React from 'react'
import { ADMIN_EMAIL, ELIGIBILITY_EMAIL } from '../config'

export function adminEmail() {
  return <a href={`mailto:${ADMIN_EMAIL}`}>{ADMIN_EMAIL}</a>
}

export function eligibilityEmail() {
  return <a href={`mailto:${ELIGIBILITY_EMAIL}`}>{ELIGIBILITY_EMAIL}</a>
}
