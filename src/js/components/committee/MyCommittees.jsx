import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Glyphicon, Grid } from 'react-bootstrap'
import { Link } from 'react-router'
import { Map } from 'immutable'
import { fetchCommittees } from '../../redux/actions/committeeActions'
import {
  isAdmin,
  isCommitteeAdmin,
  isMemberLoaded
} from '../../services/members'
import Loading from '../common/Loading'
import ConfirmCommittee from '../committee/ConfirmCommittee'

class MyCommittees extends Component {
  componentDidMount() {
    this.props.fetchCommittees()
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    const adminView =
      isAdmin(this.props.member) || isCommitteeAdmin(this.props.member)

    return (
      <Grid>
        <h1>My Committees</h1>
        <h3>Where am I active?</h3>
        {this.renderActiveCommittees()}
        {adminView && (
          <div>
            <h3>Which committees can I update?</h3>
            {this.renderAdminCommittees()}
          </div>
        )}
        <h3>How can I get involved?</h3>
        {this.renderAllCommittees()}
      </Grid>
    )
  }

  renderAdminCommittees() {
    const committeeAdminId = this.props.member
      .getIn(['user', 'data', 'roles'])
      .filter(role => role.get('role') === 'admin')
      .filter(role => role.get('committee') !== 'general')
      .map(role => role.get('committee_id'))
    const committees = this.props.committees
      .get('byId', Map())
      .valueSeq()
      .sortBy(committee => committee.get('name'))
      .filter(
        committee =>
          isAdmin(this.props.member) ||
          committeeAdminId.includes(committee.get('id'))
      )

    if (isAdmin(this.props.member)) {
      return (
        <div>
          <p>You can update any committee:</p>
          {committees.map(committee =>
            this.renderCommittee(committee, true, false)
          )}
        </div>
      )
    } else {
      return (
        <p>
          You can update{' '}
          {committees
            .map(committee => (
              <Link to={`/committees/${committee.get('id')}`}>
                <strong>{committee.get('name')}</strong>
              </Link>
            ))
            .interpose(', ')}
          .
        </p>
      )
    }
  }

  renderActiveCommittees() {
    const roles = this.props.member.getIn(['user', 'data', 'roles'])
    const committees = roles
      .filter(role => role.get('role') === 'active')
      .map(role => role.get('committee_name'))
      .filter(name => name !== 'general')

    const currentlyActive =
      committees.size === 0 ? (
        <p>You're currently not active in any committees.</p>
      ) : (
        <p>
          You're currently active in {committees.size}{' '}
          {committees.size === 1 ? 'committee' : 'committees'}:{' '}
          {committees.join(', ')}.
        </p>
      )

    return (
      <div>
        {currentlyActive}
        <p>
          If you've active been active in a committee but don't see it listed,
          send a request to the co-chairs to have them confirm:
        </p>
        <Grid>
          <Col xs={12} sm={8} md={6} lg={4}>
            <ConfirmCommittee />
          </Col>
        </Grid>
      </div>
    )
  }

  renderAllCommittees() {
    const committees = this.props.committees
      .get('byId', Map())
      .valueSeq()
      .sortBy(committee => committee.get('name'))
      .map(committee => this.renderCommittee(committee, false, true))

    return (
      <div>
        <p>
          Our chapter has numerous committees and working groups focused on
          various types of work, both internal and external. To find out more
          about what they do, check out{' '}
          <a href="https://dsasf.org/working-groups/">
            dsasf.org/working-groups
          </a>
          .
        </p>
        <p>
          If you'd like to get in touch with the leadership for a committee, use
          the list below:
        </p>
        {committees}
      </div>
    )
  }

  renderCommittee(committee, showLink, showEmailButton) {
    const id = committee.get('id')
    const name = committee.get('name')
    const email = committee.get('email')

    const emailButton =
      showEmailButton && email ? (
        <a href={`mailto:${email}`}>
          <button>
            <Glyphicon glyph="envelope" />
          </button>
        </a>
      ) : null

    const committeeText = showLink ? (
      <Link to={`/committees/${committee.get('id')}`}>{name}</Link>
    ) : (
      name
    )

    return (
      <p key={`committee-${id}`}>
        {emailButton}
        {committeeText}
      </p>
    )
  }
}

export default connect(
  state => state,
  dispatch => ({
    fetchCommittees: () => dispatch(fetchCommittees())
  })
)(MyCommittees)
