import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Form, Grid, Row } from 'react-bootstrap'
import { Link } from 'react-router'
import {
  createCommittee,
  fetchCommittees
} from '../../redux/actions/committeeActions'
import FieldGroup from '../common/FieldGroup'
import { Map } from 'immutable'
import { isAdmin } from '../../services/members'

class Committees extends Component {
  constructor(props) {
    super(props)
    this.state = {
      committee: { name: '', admin_list: '' }
    }
  }

  componentDidMount() {
    this.props.fetchCommittees()
  }

  updateForm(name, formKey, value) {
    if (
      this.props.committees.getIn(['form', 'create', 'inSubmission'], false)
    ) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({ [name]: update })
  }

  renderCommittee(committee) {
    const id = committee.get('id')
    const name = committee.get('name')

    return (
      <div key={`committee-${id}`}>
        <Link to={`/committees/${id}`}>{name}</Link>
      </div>
    )
  }

  createCommitteeFromState(committee) {
    const admin_list = (committee.admin_list || '').split(',').filter(v => v)
    this.props.createCommittee({
      name: committee.name,
      admin_list
    })
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return null
    }

    const committees = this.props.committees
      .get('byId', Map())
      .valueSeq()
      .sortBy(committee => committee.get('name'))
      .map(committee => this.renderCommittee(committee))
    return (
      <Grid>
        <Row>
          <Col sm={4}>
            <h2>Add Committee</h2>
            <Form horizontal onSubmit={e => e.preventDefault()}>
              <FieldGroup
                formKey="name"
                type="text"
                label="Committee Name"
                value={this.state.committee.name}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('committee', formKey, value)
                }
                required
              />
              <FieldGroup
                formKey="admin_list"
                type="text"
                label="Admin Emails (comma-separated)"
                value={this.state.committee.admin_list}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('committee', formKey, value)
                }
                required
              />
              <button
                type="submit"
                onClick={e => {
                  e.preventDefault()
                  this.createCommitteeFromState(this.state.committee)
                }}
              >
                Add Committee
              </button>
            </Form>
          </Col>
        </Row>
        <h2>Committees and Working Groups</h2>
        {committees}
      </Grid>
    )
  }
}

export default connect(
  state => state,
  dispatch => ({
    fetchCommittees: () => dispatch(fetchCommittees()),
    createCommittee: committee => dispatch(createCommittee(committee))
  })
)(Committees)
