import React, { Component } from 'react'
import { List, Map } from 'immutable'
import { Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { adminEmail, eligibilityEmail } from '../../services/emails'
import {
  fetchCommittee,
  markMemberInactive,
  removeAdmin
} from '../../redux/actions/committeeActions'
import { Members } from '../../client/MemberClient'
import MemberSearchField from '../common/MemberSearchField'
import { isAdmin } from '../../services/members'

class Committee extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.fetchCommittee(this.props.params.committeeId)
  }

  render() {
    const committee = this.props.committees
      .get('byId', Map())
      .get(parseInt(this.props.params.committeeId), Map())

    const isAdminFlag = isAdmin(this.props.member)
    const admins = committee
      .get('admins', List())
      .sortBy(member => member.get('name'))
      .map(member => (
        <div
          key={`committee-admin-${member.get('id')}`}
          className="committee-member"
        >
          {isAdminFlag && (
            <button
              onClick={e =>
                this.props.removeAdmin(member.get('id'), committee.get('id'))
              }
            >
              <Glyphicon glyph="remove" />
            </button>
          )}
          {member.get('name')}
        </div>
      ))

    const activeMembers = committee
      .get('active', List())
      .sortBy(member => member.get('name'))
      .map(member => (
        <div
          key={`committee-active-${member.get('id')}`}
          className="committee-member"
        >
          <button
            onClick={e =>
              this.props.markMemberInactive(
                member.get('id'),
                committee.get('id')
              )
            }
          >
            <Glyphicon glyph="remove" />
          </button>
          {member.get('name')}
        </div>
      ))

    return (
      <div>
        <h2>{committee.get('name')}</h2>
        <h3>Admins</h3>
        <p>
          This committee has {admins.size} admins. Contact {adminEmail()} to
          update this list.
        </p>
        {admins}
        <h3>Active Members</h3>
        <p>
          This committee has {activeMembers.size} active members. Use the
          buttons to mark members as inactive, or enter the member's name below
          to mark them as active. If their name is not in the membership portal,
          invite them to <a href="https://dsasf.org/join">join DSA</a> and ask
          them to forward their confirmation email to {eligibilityEmail()}.
        </p>
        {activeMembers}
        <h4>Add Active Member</h4>
        <MemberSearchField onMemberSelected={e => this.onMemberSelected(e)} />
      </div>
    )
  }

  async onMemberSelected(member) {
    const memberId = member.id
    const committeeId = parseInt(this.props.params.committeeId, 10)

    const committee = this.props.committees
      .get('byId', Map())
      .get(committeeId, Map())
    const existingMember = committee
      .get('members', List())
      .find(member => member.get('id') === memberId)
    if (existingMember === null) {
      await Members.addRole(memberId, 'member', committeeId)
    }
    await Members.addRole(memberId, 'active', committeeId)

    this.props.fetchCommittee(committeeId)
  }
}

export default connect(
  state => state,
  dispatch =>
    bindActionCreators(
      { fetchCommittee, markMemberInactive, removeAdmin },
      dispatch
    )
)(Committee)
