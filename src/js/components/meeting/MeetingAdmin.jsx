import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormControl,
  FormGroup
} from 'react-bootstrap'
import { Map } from 'immutable'
import DateTime from 'react-datetime'
import { Meetings } from '../../client/MeetingClient'
import { Members } from '../../client/MemberClient'
import MemberSearchField from '../common/MemberSearchField'
import { AttendeeEligibilityRow, EmptyEligibilityRow } from '../admin/MemberRow'

class MeetingAdmin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      meeting: Map({ id: 0, name: 'Loading...' }),
      attendees: null,
      inSubmission: false
    }
  }

  componentDidMount() {
    this.getMeeting()
    this.getMeetingAttendees()
  }

  render() {
    const attendees = this.state.attendees

    const attendeesView = attendees
      ? attendees.size
        ? attendees.map((attendee, index) => {
            return (
              <div key={`attendee-${index}`}>
                <span>{`${attendee.get('name')}`}</span>
                &nbsp;
                <a
                  href="javascript:void(0)"
                  className="attendee-remove"
                  onClick={e => this.removeAttendeeClicked(attendee)}
                >
                  <i>remove</i>
                </a>
                &nbsp;
                <a
                  href="javascript:void(0)"
                  className="attendee-update-eligibility-to-vote"
                  onClick={e =>
                    this.updateAttendeeClicked(attendee, {
                      update_eligibility_to_vote: true
                    })
                  }
                >
                  <i>update eligibility to vote</i>
                </a>
                {this.isGeneralMeeting() ? (
                  attendee.hasIn(['eligibility']) ? (
                    <AttendeeEligibilityRow
                      isPersonallyEligible={attendee.getIn([
                        'eligibility',
                        'is_eligible'
                      ])}
                      numVotes={attendee.getIn(
                        ['eligibility', 'num_votes'],
                        // if num_votes was not passed, assume is_eligible counts as 1
                        attendee.getIn(['eligibility', 'is_eligible']) ? 1 : 0
                      )}
                    />
                  ) : (
                    <EmptyEligibilityRow />
                  )
                ) : null}
              </div>
            )
          })
        : 'None'
      : 'Loading...'

    return (
      <div className="meeting-admin">
        <h2>
          {this.state.meeting.get('name')}{' '}
          <small>
            {this.state.meeting.get('code')
              ? '#' + this.state.meeting.get('code')
              : ''}
          </small>
        </h2>
        <h3>Edit Meeting</h3>
        <Form horizontal onSubmit={e => e.updateLandingUrl()}>
          <FormGroup controlId="formLandingUrl">
            <Col componentClass={ControlLabel} sm={2}>
              Landing URL
            </Col>
            <Col sm={6}>
              <FormControl
                type="text"
                value={this.state.meeting.get('landing_url')}
                onChange={e => this.updateLandingUrl(e.target.value)}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              Start Time
            </Col>
            <Col sm={6}>
              <DateTime
                value={this.state.meeting.get('start_time')}
                onChange={m => this.updateStartTime(m)}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              End Time
            </Col>
            <Col sm={6}>
              <DateTime
                value={this.state.meeting.get('end_time')}
                onChange={m => this.updateEndTime(m)}
              />
            </Col>
          </FormGroup>
          <button type="submit" onClick={e => this.submitMeeting(e)}>
            Submit
          </button>
        </Form>
        <h3>Sign In</h3>
        <MemberSearchField onMemberSelected={e => this.onMemberSelected(e)} />
        <h3>{'Attendees ' + (attendees ? '(' + attendees.size + ')' : '')}</h3>
        {this.isGeneralMeeting() && attendees != null ? (
          <small>
            <i>Eligible voters: {this.attendeesEligibleCount(attendees)}</i>
            <br />
            <i>
              Number of proxy votes: {this.proxyVotesCount(attendees)} (already
              included in Eligible voter count)
            </i>
          </small>
        ) : null}
        <br />
        {attendeesView}
      </div>
    )
  }

  updateLandingUrl(landingUrl) {
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      meeting: this.state.meeting.set('landing_url', landingUrl)
    })
  }

  updateStartTime(startTime) {
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      meeting: this.state.meeting.set('start_time', startTime)
    })
  }

  updateEndTime(endTime) {
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      meeting: this.state.meeting.set('end_time', endTime)
    })
  }

  attendeesEligibleCount(attendees) {
    return attendees.filter(
      attendee =>
        attendee.hasIn(['eligibility', 'is_eligible'], false) &&
        attendee.getIn(['eligibility', 'is_eligible'])
    ).size
  }

  proxyVotesCount(attendees) {
    return attendees.reduce((result, attendee) => {
      const isPersonallyEligible =
        attendee.hasIn(['eligibility', 'is_eligible'], false) &&
        attendee.getIn(['eligibility', 'is_eligible'])
      const numVotes = attendee.getIn(
        ['eligibility', 'num_votes'],
        // if num_votes was not passed, assume is_eligible counts as 1
        isPersonallyEligible ? 1 : 0
      )
      const expectedNumVotes = isPersonallyEligible ? 1 : 0
      const numProxyVotes = numVotes - expectedNumVotes
      return result + numProxyVotes
    }, 0)
  }

  isGeneralMeeting() {
    return (
      Boolean(this.state.meeting.get('name').match(/General/)) &&
      this.state.meeting.get('committee_id') == null
    )
  }

  async submitMeeting(e) {
    e.preventDefault()

    if (this.state.inSubmission) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      const attributeNames = ['landing_url', 'start_time', 'end_time']
      const updatedAttributes = this.state.meeting
        // filter out attribute names we don't want to send up
        .filter((_, key) => attributeNames.includes(key))
        // treat falsy values as null when sending up
        .map((val, _) => val || null)
      // send up ISO 8601 strings
      if (updatedAttributes.includes('start_time')) {
        updatedAttributes['start_time'] = updatedAttributes[
          'start_time'
        ].toISOString()
      }
      if (updatedAttributes.includes('end_time')) {
        updatedAttributes['end_time'] = updatedAttributes[
          'end_time'
        ].toISOString()
      }

      const result = await Meetings.updateMeeting(
        this.state.meeting.get('id'),
        updatedAttributes
      )
      // parse received ISO 8601 strings
      let meeting = result.get('meeting')
      if (meeting.get('start_time')) {
        meeting = meeting.set('start_time', new Date(meeting.get('start_time')))
      }
      if (meeting.get('end_time')) {
        meeting = meeting.set('end_time', new Date(meeting.get('end_time')))
      }
      const updatedMeeting = this.state.meeting.merge(meeting)
      this.setState({ meeting: updatedMeeting })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  async onMemberSelected(member) {
    await Meetings.addAttendee(this.props.params.meetingId, member.id)
    this.getMeetingAttendees()
  }

  async getMeetingAttendees() {
    await this.getMeeting()

    const meetingId = this.props.params.meetingId
    const results = await Meetings.getAttendees(
      meetingId,
      this.isGeneralMeeting()
    )
    this.setState({ attendees: results })
  }

  async updateAttendeeClicked(member, attributes) {
    await Meetings.updateAttendee(
      this.props.params.meetingId,
      member.get('id'),
      attributes
    )
    this.getMeetingAttendees()
  }

  async removeAttendeeClicked(member) {
    const meetingName = this.state.meeting.get('name')
    if (
      confirm(
        `Are you sure you want to remove ${member.get(
          'name'
        )} from the list of attendees for ${meetingName}?`
      )
    ) {
      await Meetings.removeAttendee(
        this.props.params.meetingId,
        member.get('id')
      )
      this.getMeetingAttendees()
    }
  }

  async getMeeting() {
    let meeting = await Meetings.get(this.props.params.meetingId)
    // parse received ISO 8601 strings
    if (meeting.get('start_time')) {
      meeting = meeting.set('start_time', new Date(meeting.get('start_time')))
    }
    if (meeting.get('end_time')) {
      meeting = meeting.set('end_time', new Date(meeting.get('end_time')))
    }
    this.setState({ meeting: meeting })
  }
}

export default connect(state => state)(MeetingAdmin)
