import React, { Component } from 'react'
import CreateMeeting from './CreateMeeting'
import MeetingList from './MeetingList'

export default class Meetings extends Component {
  render() {
    return (
      <div>
        <CreateMeeting />
        <h2> Meetings </h2>
        <MeetingList />
      </div>
    )
  }
}
