import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isAdmin } from '../../services/members'
import { Elections } from '../../client/ElectionClient'
import { logError } from '../../util/util'
import { fromJS, List, Map } from 'immutable'

class ElectionResults extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inSubmission: false,
      results: Map()
    }
  }

  updateForm(name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({ [name]: update })
  }

  render() {
    const admin = isAdmin(this.props.member)
    const winners = this.state.results
      .get('winners', List())
      .map((winner, index) => {
        const candidateID = winner.get('candidate').toString()
        const candidateDetails = this.state.results
          .get('candidates')
          .get(candidateID)
        return <div key={index}>{candidateDetails.get('name')}</div>
      })

    return admin ? (
      <div>
        <div>
          <button type="submit" onClick={e => this.countVotes(e)}>
            Count the Vote
          </button>
        </div>
        {this.state.results.size > 0 && (
          <div>
            <h3>Winners</h3>
            {winners}
            <h3>Counts</h3>
            <div>{this.state.results.get('ballot_count')} ballots cast</div>
          </div>
        )}
      </div>
    ) : (
      <div />
    )
  }

  async countVotes(e) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const results = await Elections.countResults(this.props.params.electionId)
      this.setState({ results: fromJS(results) })
    } catch (err) {
      return logError('Error counting votes', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect(state => state)(ElectionResults)
