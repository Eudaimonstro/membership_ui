import { Map } from 'immutable'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Form, Grid, Row } from 'react-bootstrap'
import { bindActionCreators } from 'redux'
import { isMemberLoaded } from '../../services/members'
import { Elections } from '../../client/ElectionClient'
import {
  fetchElection,
  fetchElections
} from '../../redux/actions/electionActions'
import Loading from '../common/Loading'
import FieldGroup from '../common/FieldGroup'

class ViewVote extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    this.props.fetchElections()
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    const votes = this.props.member
      .getIn(['user', 'data', 'votes'])
      .filter(vote => vote.get('voted'))

    const body =
      votes.size === 0 ? (
        <p>You haven't voted in any elections before.</p>
      ) : (
        this.renderBody(votes)
      )
    return (
      <Grid>
        <Row>
          <h2>Check My Vote</h2>
        </Row>
        {body}
      </Grid>
    )
  }

  renderBody(votes) {
    const electionsById = Map(
      votes.map(vote => [vote.get('election_id'), vote.get('election_name')])
    )

    return (
      <Form horizontal onSubmit={e => e.preventDefault()}>
        <Row>
          <p>
            To check how you voted in an election, enter the 6-digit
            confirmation number. Note that once you've voted, your vote cannot
            be changed.
          </p>
        </Row>
        <Row>
          <Col xs={12} sm={8} md={6}>
            <FieldGroup
              formKey="election_id"
              componentClass="select"
              options={Map([[0, '']]).merge(electionsById)}
              optionMap
              label="Election"
              onFormValueChange={(formKey, value) => {
                this.electionId = parseInt(value, 10)
              }}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={8} md={6}>
            <div className="form-group">
              <label htmlFor="code" className="col-sm-3 control-label">
                Confirmation Number
              </label>
              <Col sm={9}>
                <input
                  id="code"
                  type="text"
                  placeholder="000000"
                  maxLength="6"
                  onChange={e => {
                    this.code = e.target.value
                  }}
                />
              </Col>
            </div>
          </Col>
        </Row>
        <Row>
          <button type="button" onClick={() => this.fetchVote()}>
            Check vote
          </button>
        </Row>
        {this.renderResult()}
      </Form>
    )
  }

  async fetchVote() {
    const electionId = this.electionId
    const code = this.code

    // Make an asynchronous call to fetch the candidates.
    this.props.fetchElection(electionId)

    try {
      const vote = await Elections.getVote(electionId, code)
      this.setState({ result: vote.set('found', true) })
    } catch (err) {
      const result = Map({ election_id: electionId, code: code, found: false })
      this.setState({ result })
    }
  }

  renderResult() {
    if (this.state.result == null) {
      return null
    }

    const electionId = this.state.result.get('election_id')
    const election = this.props.elections.getIn(['byId', electionId])
    const electionName = election.get('name')

    if (!this.state.result.get('found')) {
      return (
        <div>
          <h3>Your vote was not found.</h3>
          <p>
            We couldn't find a vote on <strong>{electionName}</strong> with the
            confirmation code <strong>{this.state.result.get('code')}</strong>.
          </p>
        </div>
      )
    } else {
      const rankings = this.state.result.get('rankings')
      const candidates = election.get('candidates')

      let candidateList
      if (candidates == null) {
        candidateList = <p>Loading...</p>
      } else if (rankings.size === 0) {
        candidateList = <p>You did not select any candidates.</p>
      } else if (rankings.size === 1) {
        const candidate = candidates.find(c => c.get('id') === rankings.first())
        candidateList = (
          <p>
            You selected <strong>{candidate.get('name')}</strong>.
          </p>
        )
      } else {
        candidateList = (
          <div>
            <p>
              These are your rankings for <strong>{electionName}</strong>:
            </p>
            <ol>
              {rankings.map(ranking => {
                const candidate = candidates.find(c => c.get('id') === ranking)
                return (
                  <li key={`candidate-${candidate.get('id')}`}>
                    {candidate.get('name')}
                  </li>
                )
              })}
            </ol>
          </div>
        )
      }

      return (
        <div>
          <h3>We got your vote!</h3>
          {candidateList}
        </div>
      )
    }
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({ fetchElection, fetchElections }, dispatch)
)(ViewVote)
