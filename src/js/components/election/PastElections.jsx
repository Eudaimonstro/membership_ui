import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { electionStatus } from '../../services/elections'

class PastElections extends Component {
  render() {
    const allVotes = this.props.member.getIn(['user', 'data', 'votes'])
    const pastVotes = allVotes.filter(vote =>
      this.isPastElection(vote.get('election_id'))
    )

    if (pastVotes.size === 0) {
      return <p>You haven't been eligible to vote on anything before.</p>
    } else {
      return <div>{pastVotes.reverse().map(vote => this.renderVote(vote))}</div>
    }
  }

  renderVote(vote) {
    const electionId = vote.get('election_id')
    const electionName = this.getElection(electionId).get('name')

    const electionLink = (
      <Link to={`/elections/${electionId}`}>{electionName}</Link>
    )

    if (vote.get('voted')) {
      return (
        <p key={`election-${electionId}`}>
          You voted on <strong>{electionLink}</strong>.
        </p>
      )
    } else {
      return (
        <p key={`election-${electionId}`}>
          There was a vote on <strong>{electionLink}</strong>.
        </p>
      )
    }
  }

  getElection(electionId) {
    return this.props.elections.getIn(['byId', electionId])
  }

  isPastElection(electionId) {
    const election = this.getElection(electionId)
    return (
      election &&
      (electionStatus(election) === 'final' ||
        electionStatus(election) === 'polls closed')
    )
  }
}

export default connect(state => state)(PastElections)
