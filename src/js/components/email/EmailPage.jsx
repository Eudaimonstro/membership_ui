import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { fetchEmailTemplates } from '../../redux/actions/emailTemplateActions'
import { List } from 'immutable'
import { isAdmin } from '../../services/members'

class EmailPage extends Component {
  componentDidMount() {
    this.props.fetchEmailTemplates()
  }

  render() {
    const templates = this.props.templates.get('byId')
    const templateLinks = this.getTemplateLinks(templates)
    return (
      <div style={{ paddingLeft: '32px' }}>
        <h2>Email Templates</h2>
        <ul>{templateLinks}</ul>
        <br />
        <br />
        <button
          type="button"
          name="newTemplate"
          onClick={() => this.createNewTemplate(templates.size)}
        >
          New Template
        </button>
      </div>
    )
  }

  getTemplateLinks(templates) {
    return !isAdmin(this.props.member)
      ? List()
      : templates.valueSeq().map(tmpl => {
          const linkTo = {
            pathname: `/email/template/${tmpl.get('id')}`,
            state: tmpl
          }
          return (
            <li key={tmpl.get('id')}>
              <Link to={linkTo}>{tmpl.get('name')}</Link>
            </li>
          )
        })
  }

  createNewTemplate(templateCount) {
    this.props.router.push({
      pathname: 'email/newTemplate',
      state: { template_count: templateCount }
    })
  }
}

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => ({
  fetchEmailTemplates: () => dispatch(fetchEmailTemplates())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmailPage)
