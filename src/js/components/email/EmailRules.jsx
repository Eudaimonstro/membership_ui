import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Col, FormControl, Row } from 'react-bootstrap'
import { isAdmin } from '../../services/members'
import * as emailRuleActions from '../../redux/actions/emailRuleActions'

class EmailRules extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inSubmission: false
    }
  }

  componentDidMount() {
    this.props.fetchEmailRules()
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return <div />
    }
    const emails = []
    this.props.emails.get('byId').forEach((email, emailIndex) => {
      let forwardingAddresses = []
      let button = null
      if (!this.state.inSubmission) {
        if (email.get('status', null) === 'new') {
          button = (
            <Col sm={3}>
              <button onClick={e => this.props.saveEmailRule(email)}>
                Add Email
              </button>
            </Col>
          )
        } else if (email.get('status', null) === 'modified') {
          button = (
            <Col sm={3}>
              <button
                style={{ marginTop: '8px' }}
                onClick={e => this.props.saveEmailRule(email)}
              >
                Modify Email
              </button>
            </Col>
          )
        }
      }
      email
        .get('forwarding_addresses')
        .forEach((forwardingAddress, forwardingIndex) => {
          forwardingAddresses.push(
            <div
              style={{ marginTop: '8px' }}
              key={`forward-${emailIndex}-${forwardingIndex}`}
              className="form-inline"
            >
              <FormControl
                type="text"
                value={forwardingAddress}
                onChange={e =>
                  this.props.updateForwardingAddress(
                    emailIndex,
                    forwardingIndex,
                    e.target.value
                  )
                }
              />
              <button
                onClick={e =>
                  this.props.deleteForwardingAddress(
                    emailIndex,
                    forwardingIndex
                  )
                }
              >
                x
              </button>
            </div>
          )
        })
      emails.push(
        <Row key={`email-admin-${emailIndex}`}>
          <Row style={{ paddingTop: '8px' }}>
            <Col sm={4}>Incoming Email</Col>
            <Col sm={4}>Forwards to:</Col>
          </Row>
          <Row>
            <Col sm={4} className="form-inline">
              <div style={{ marginTop: '8px' }} className="form-inline">
                <button onClick={e => this.deleteEmail(emailIndex)}>x</button>
                <FormControl
                  type="text"
                  value={email.get('email_address')}
                  onChange={e =>
                    this.props.updateEmailAddress(emailIndex, e.target.value)
                  }
                />
              </div>
            </Col>
            <Col sm={4}>
              {forwardingAddresses}
              <button
                style={{ marginTop: '8px' }}
                onClick={e => this.props.addForwardingAddress(emailIndex)}
              >
                +
              </button>
            </Col>
            {button}
          </Row>
        </Row>
      )
    })
    return (
      <div style={{ paddingLeft: '32px', paddingBottom: '48px' }}>
        <h2>Emails </h2>
        {emails}
        <button
          style={{ marginTop: '16px' }}
          type="submit"
          onClick={e => this.props.newEmailRule()}
        >
          Add New Email
        </button>
      </div>
    )
  }
}

const mapStateToProps = state => state

const mapDispatchToProps = dispatch =>
  bindActionCreators(emailRuleActions, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmailRules)
