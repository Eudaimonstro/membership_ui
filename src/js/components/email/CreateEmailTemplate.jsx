import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fromJS } from 'immutable'
import { bindActionCreators } from 'redux'
import { saveEmailTemplate } from '../../redux/actions/emailTemplateActions'
import { fetchEmailTopics } from '../../redux/actions/emailTopicActions'

class CreateEmailTemplate extends Component {
  constructor(props) {
    super(props)
    const map = {
      name: '',
      subject: '',
      body: '',
      topic_id: ''
    }
    this.state = { template: fromJS(map) }
    this.props.fetchEmailTopics()
  }

  save() {
    const { template } = this.state
    if (
      template.get('name') === '' ||
      template.get('subject') === '' ||
      template.get('body') === ''
    ) {
      // do nothing
    } else {
      this.props.saveEmailTemplate(template)
    }
  }

  handleNameChange(event) {
    const { template } = this.state
    this.setState({
      template: fromJS({
        name: event.target.value,
        subject: template.get('subject'),
        body: template.get('body'),
        topic_id: this.getSelectedTopic()
      })
    })
  }

  handleSubjectChange(event) {
    const { template } = this.state
    this.setState({
      template: fromJS({
        name: template.get('name'),
        subject: event.target.value,
        body: template.get('body'),
        topic_id: this.getSelectedTopic()
      })
    })
  }

  handleBodyChange(event) {
    const { template } = this.state
    this.setState({
      template: fromJS({
        name: template.get('name'),
        subject: template.get('subject'),
        body: event.target.value,
        topic_id: this.getSelectedTopic()
      })
    })
  }

  getSelectedTopic() {
    const select = document.getElementById('topicsSelect')
    return select.options[select.selectedIndex].value
  }

  render() {
    const topics = this.getTopics()
    return (
      <div style={{ paddingLeft: '32px' }}>
        <h2>New Template</h2>
        Topic / Recipients:
        <select id="topicsSelect" style={{ marginLeft: 10 }}>
          {topics}
        </select>
        <br />
        <br />
        Title
        <br />
        <textarea
          name="title"
          rows="1"
          cols="60"
          onChange={e => this.handleNameChange(e)}
        />
        <br />
        <br />
        Subject
        <br />
        <textarea
          name="subject"
          rows="1"
          cols="60"
          onChange={e => this.handleSubjectChange(e)}
        />
        <br />
        <br />
        Body
        <br />
        <textarea
          name="body"
          rows="10"
          onChange={e => this.handleBodyChange(e)}
        />
        <br />
        <br />
        <button type="button" name="save" onClick={() => this.save()}>
          Save
        </button>
        <br />
        <br />
      </div>
    )
  }

  getTopics() {
    const topics = this.props.topics.get('byId')
    return topics.valueSeq().map(topic => {
      return <option value={topic.get('id')}>{topic.get('name')}</option>
    })
  }
}

const mapStateToProps = (state, props) => {
  if (state.templates.size > 0) {
    const templates = state.templates.get('byId')
    if (templates.size > props.location.state.template_count) {
      const highestId = getNewId(templates)
      const template = templates.get(highestId)
      props.router.replace({
        pathname: `/email/template/${template.get('id')}`,
        state: template
      })
    }
  }
  return state
}

function getNewId(templates) {
  return templates.maxBy(t => t.get('id') || fromJS({ id: -1 })).get('id')
}

export default connect(
  mapStateToProps,
  dispatch =>
    bindActionCreators({ saveEmailTemplate, fetchEmailTopics }, dispatch)
)(CreateEmailTemplate)
