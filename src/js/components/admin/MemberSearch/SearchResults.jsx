import React from 'react'
import SearchResultRow from './SearchResultRow'

const SearchResults = ({ isLoading, results, hasMoreResults, loadMore }) => (
  <div className="admin-search-list">
    {results.map(member => (
      <SearchResultRow key={member.get('id')} member={member} />
    ))}
    {isLoading && <span>Loading...</span>}
    {hasMoreResults &&
      !isLoading && (
        <button className="admin-search-load-more" onClick={loadMore}>
          Load more
        </button>
      )}
  </div>
)

export default SearchResults
