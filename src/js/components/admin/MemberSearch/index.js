import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Row, Col } from 'react-bootstrap'

import Actions from '../../../redux/actions/memberSearchActions'

import SearchInput from './SearchInput'
import SearchResults from './SearchResults'

class MemberSearch extends React.Component {
  constructor(props) {
    super(props)
    this.handleLoadMore = this.handleLoadMore.bind(this)
  }
  handleLoadMore() {
    const { loadMoreResults, memberSearch } = this.props
    loadMoreResults(memberSearch.get('query'), memberSearch.get('cursor'))
  }
  render() {
    const {
      memberSearch,
      updateMemberSearchInput,
      clearMemberSearchInput,
      searchMembers
    } = this.props
    return (
      <div className="admin-member-search">
        <Row>
          <Col sm={4}>
            <h2>Search Members</h2>
            <SearchInput
              {...memberSearch.toObject()}
              onChange={updateMemberSearchInput}
              onClear={clearMemberSearchInput}
              onSearch={searchMembers}
            />
            <SearchResults
              {...memberSearch.toObject()}
              loadMore={this.handleLoadMore}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({ memberSearch: state.memberSearch })
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MemberSearch)
