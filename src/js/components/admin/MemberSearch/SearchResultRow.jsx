import React from 'react'
import { AttendeeEligibilityRow, EmptyEligibilityRow } from '../MemberRow'
import { Link } from 'react-router'

const SearchResultRow = ({ member }) => {
  return (
    <div key={`member-${member.get('id')}`}>
      <Link to={`/members/${member.get('id')}`}>
        {`${member.get('name')}: ${member.get('email')}`}
      </Link>
      {member.hasIn(['eligibility', 'is_eligible'], false) ? (
        <AttendeeEligibilityRow
          isPersonallyEligible={member.getIn(['eligibility', 'is_eligible'])}
          message={member.getIn(['eligibility', 'message'])}
        />
      ) : (
        <EmptyEligibilityRow />
      )}
    </div>
  )
}

export default SearchResultRow
