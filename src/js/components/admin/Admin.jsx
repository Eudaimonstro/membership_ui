import React, { Component } from 'react'
import { Link } from 'react-router'
import { Col, Grid, Row } from 'react-bootstrap'

export default class Admin extends Component {
  render() {
    return (
      <Grid>
        <Row>
          <h1>Admin Tools</h1>
        </Row>
        <Row>
          <Col xs={12} sm={6}>
            <h3>Members</h3>
            <p>
              <Link to={`/members`}>Member List</Link>
            </p>
            <p>
              <Link to={`/import`}>Import Roster</Link>
            </p>
          </Col>
          <Col xs={12} sm={6}>
            <h3>Elections</h3>
            <p>
              <Link to={`/elections`}>Manage Elections</Link>
            </p>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6}>
            <h3>Meetings</h3>
            <p>
              <Link to={`/meetings`}>Manage Meetings</Link>
            </p>
          </Col>
          <Col xs={12} sm={6}>
            <h3>Emails</h3>
            <p>
              <Link to={`/email/rules`}>Email Rules</Link>
            </p>
            <p>
              <Link to={`/email`}>Email Templates</Link>
            </p>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6}>
            <h3>Committees</h3>
            <p>
              <Link to={`/committees`}>Add Committee</Link>
            </p>
          </Col>
        </Row>
      </Grid>
    )
  }
}
