import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../redux/actions/callback'
import { logError } from '../../util/util'

import PropTypes from 'prop-types'

class Callback extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.contextTypes = {
      router: PropTypes.object
    }
    this.props.loading()
    this.forceUpdate()
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.loading === false
  }

  componentWillUpdate(nextProps, nextState) {
    this.props.signoutUser()
    if (/access_token|id_token|error/.test(nextProps.location.hash)) {
      this.props
        .handleAuthentication()
        .then(result => {
          // Check local storage for a nonce and redirect
          const storedState = localStorage.getItem(result.state)
          if (storedState) {
            try {
              const { redirectPath } = JSON.parse(storedState)
              localStorage.removeItem(result.state)
              return this.props.router.push(redirectPath)
            } catch (err) {
              // If json parse fails, just fall through.
              logError('Parsing error during post login redirect', err)
            }
          }
          return this.props.router.push('/home')
        })
        .catch(() => {
          this.props.router.push('/')
        })
    } else {
      this.props.router.push('/home')
    }
  }

  render() {
    return <div />
  }
}

const mapStateToProps = state => {
  return state
}

export default connect(
  mapStateToProps,
  actions
)(Callback)
