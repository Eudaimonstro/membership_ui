import React, { Component } from 'react'
import Autosuggest from 'react-autosuggest'
import { Members } from '../../client/MemberClient'

const getSuggestionValue = suggestion => suggestion.name

export default class MemberSearchField extends Component {
  constructor(props) {
    super(props)
    this.suggestionsCache = {}
    this.state = {
      suggestions: [],
      searchInput: ''
    }
  }

  render() {
    const inputProps = {
      placeholder: 'Name or email address',
      className: 'form-control member-search',
      value: this.state.searchInput,
      onChange: (e, args) => this.onChange(e, args)
    }

    const renderMember = (member, { query, isHighlighted }) => (
      <div className={isHighlighted ? 'suggestion-highlighted' : ''}>
        <a href="javascript:void(0)">{member.name}</a>
      </div>
    )

    return (
      <Autosuggest
        suggestions={this.state.suggestions}
        onSuggestionsFetchRequested={e => this.onSuggestionsFetchRequested(e)}
        onSuggestionsClearRequested={e => this.onSuggestionsClearRequested()}
        onSuggestionSelected={(e, args) => this.onSuggestionSelected(e, args)}
        highlightFirstSuggestion={true}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderMember}
        inputProps={inputProps}
      />
    )
  }

  onChange(event, args) {
    this.setState({
      searchInput: args.newValue
    })
  }

  async onSuggestionsFetchRequested(event) {
    const inputValue = event.value.trim()
    const inputLength = inputValue.length

    if (inputLength === 0) {
      this.onSuggestionsClearRequested()
    } else {
      if (!this.suggestionsCache[inputValue]) {
        const results = await Members.search(inputValue)
        const members = results.toJS().members
        this.suggestionsCache[inputValue] = members
      }
      const suggestions = this.suggestionsCache[inputValue]
      this.setState({
        suggestions: suggestions
      })
    }
  }

  onSuggestionsClearRequested() {
    this.setState({
      suggestions: []
    })
  }

  async onSuggestionSelected(
    event,
    { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }
  ) {
    const member = suggestion
    this.setState({ searchInput: '' })
    this.props.onMemberSelected(member)
  }
}
