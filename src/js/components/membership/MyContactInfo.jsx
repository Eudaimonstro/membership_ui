import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Grid } from 'react-bootstrap'
import { eligibilityEmail } from '../../services/emails'
import { isMember, isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import ContactPreferences from './ContactPreferences'

class MyContactInfo extends Component {
  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    return (
      <Grid>
        <h1>My Contact Info</h1>
        {this.renderContactInfo()}
        {this.renderDoNotContact()}
      </Grid>
    )
  }

  renderContactInfo() {
    const memberData = this.props.member.getIn(['user', 'data'])

    const email = memberData.getIn(['info', 'email_address'])
    const emailMessage = (
      <p>
        Your email address is <strong>{email}</strong>. Use this to sign into
        the membership portal and the{' '}
        <a href="https://forum.dsasf.org">forum</a>.
      </p>
    )

    if (!isMember(this.props.member)) {
      return (
        <div>
          <h3>What contact info do you have for me?</h3>
          {emailMessage}
          <p>
            You're currently not listed as a member of our chapter, so we
            haven't received additional info for you.
          </p>
          <h3>How can I register?</h3>
          <p>
            If you'd like to sign up, visit{' '}
            <a href="https://dsausa.org/join">dsausa.org/join</a> and forward
            your confirmation email to {eligibilityEmail()}.
          </p>
        </div>
      )
    }

    const membership = memberData.get('membership')
    if (membership) {
      const address = membership
        .get('address')
        .map(line => <strong>{line}</strong>)
      const phoneNumbers = membership
        .get('phone_numbers')
        .map(phoneNumber => <strong>{phoneNumber}</strong>)

      return (
        <div>
          <h3>What contact info do you have for me?</h3>
          {emailMessage}
          <p>This is the address we have for you:</p>
          <p>{address.interpose(<br />)}</p>
          <p>
            If your address has changed, notify National DSA at{' '}
            <a href="https://act.dsausa.org/survey/mailingaddr">
              act.dsausa.org/survey/mailingaddr
            </a>
            , and they'll update us in the next few weeks.
          </p>
          <p>
            We have the following phone number(s) for you:{' '}
            {phoneNumbers.interpose(', ')}.
          </p>
        </div>
      )
    } else {
      return (
        <div>
          <h3>What contact info do you have for me?</h3>
          {emailMessage}
          <p>
            We don't have any other information associated with your membership.
          </p>
          <p>
            This most commonly happens for new members or people who live
            outside of San Francisco. If your address has changed, notify
            National DSA at{' '}
            <a href="https://act.dsausa.org/survey/mailingaddr">
              act.dsausa.org/survey/mailingaddr
            </a>
            , and they'll update us in the next few weeks.
          </p>
        </div>
      )
    }
  }

  renderDoNotContact() {
    return (
      <div>
        <h3>How will I be contacted?</h3>
        <p>
          Update your preferences to let us know how you prefer to receive
          communications.
        </p>
        <ContactPreferences />
        <p>
          If you also receive emails through the newsletter or Google Group, use
          the links in the footer to manage your email preferences.
        </p>
      </div>
    )
  }
}

export default connect(state => state)(MyContactInfo)
