import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Map } from 'immutable'
import { Checkbox } from 'react-bootstrap'
import { updateMember } from '../../redux/actions/memberActions'

class ContactPreferences extends Component {
  render() {
    const memberData = this.props.member.getIn(['user', 'data'])

    return (
      <div>
        <Checkbox
          checked={!memberData.get('do_not_email')}
          onChange={event => this.updateMember('do_not_email', event)}
        >
          Receive emails from DSA SF
        </Checkbox>
        <Checkbox
          checked={!memberData.get('do_not_call')}
          onChange={event => this.updateMember('do_not_call', event)}
        >
          Receive calls or texts from DSA SF
        </Checkbox>
      </div>
    )
  }

  updateMember(attribute, event) {
    const newValue = !event.target.checked
    this.props.updateMember(Map([[attribute, newValue]]))
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({ updateMember }, dispatch)
)(ContactPreferences)
