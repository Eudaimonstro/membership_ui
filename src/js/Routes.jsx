import React, { Component } from 'react'
import { Router, Route, IndexRedirect } from 'react-router'
import { connect } from 'react-redux'

import App from './components/App'
import MainPage from './components/membership/MainPage'
import Committee from './components/committee/Committee'
import Meetings from './components/meeting/Meetings'
import MyMeetings from './components/meeting/MyMeetings'
import MeetingKiosk from './components/meeting/MeetingKiosk'
import MeetingAdmin from './components/meeting/MeetingAdmin'
import ProxyTokenCreate from './components/meeting/ProxyTokenCreate'
import ProxyTokenConsume from './components/meeting/ProxyTokenConsume'
import MemberPage from './components/membership/MemberPage'
import MyContactInfo from './components/membership/MyContactInfo'
import MyMembership from './components/membership/MyMembership'
import MemberList from './components/admin/MemberList'
import ImportRoster from './components/admin/ImportRoster'
import Admin from './components/admin/Admin'
import Committees from './components/committee/Committees'
import MyCommittees from './components/committee/MyCommittees'
import MyElections from './components/election/MyElections'
import Elections from './components/election/Elections'
import ElectionDetail from './components/election/ElectionDetail'
import ElectionEdit from './components/election/ElectionEdit'
import NotFound from './components/common/NotFound'
import EnterVote from './components/election/EnterVote'
import PrintBallots from './components/election/PrintBallots'
import SignInKiosk from './components/election/SignInKiosk'
import Vote from './components/election/Vote'
import ViewVote from './components/election/ViewVote'
import EmailRules from './components/email/EmailRules'
import EmailPage from './components/email/EmailPage'
import CreateEmailTemplate from './components/email/CreateEmailTemplate'
import MyResources from './components/resources/MyResources'
import EmailTemplateDetails from './components/email/EmailTemplateDetails'
import Callback from './components/callback/Callback'

import { RankedChoiceVisualization } from './components/election/RankedChoiceVisualization'
import { signoutUser } from './redux/actions/auth/index'
import RequireAuth from './components/auth/requireAuth'
import Signin from './components/auth/signin'

class Routes extends Component {
  shouldComponentUpdate() {
    return false
  }

  render() {
    return (
      <Router history={this.props.history}>
        <Route path="/" component={App}>
          <IndexRedirect to="home" />
          <Route path="process" component={Callback} />
          <Route path="login" component={Signin} />
          <Route
            path="logout"
            onEnter={() => {
              this.props.signOutUser()
            }}
            component={Signin}
          />

          <Route>
            <Route path="home" component={RequireAuth(MainPage)} />
            <Route path="member" component={RequireAuth(MemberPage)} />
            <Route
              path="members/:memberId"
              component={RequireAuth(MemberPage)}
            />
            <Route path="meetings" component={RequireAuth(Meetings)} />
            <Route
              path="meetings/:meetingId/kiosk"
              component={RequireAuth(MeetingKiosk)}
            />
            <Route
              path="meetings/:meetingId/proxy-token"
              component={RequireAuth(ProxyTokenCreate)}
            />
            <Route
              path="meetings/:meetingId/proxy-token/:proxyTokenId"
              component={RequireAuth(ProxyTokenConsume)}
            />
            <Route
              path="meetings/:meetingId/admin"
              component={RequireAuth(MeetingAdmin)}
            />
            <Route path="committees" component={RequireAuth(Committees)} />
            <Route
              path="committees/:committeeId"
              component={RequireAuth(Committee)}
            />
            <Route path="members" component={RequireAuth(MemberList)} />
            <Route path="import" component={RequireAuth(ImportRoster)} />
            <Route path="admin" component={RequireAuth(Admin)} />
            <Route path="my-elections" component={RequireAuth(MyElections)} />
            <Route path="my-membership" component={RequireAuth(MyMembership)} />
            <Route path="my-committees" component={RequireAuth(MyCommittees)} />
            <Route path="my-meetings" component={RequireAuth(MyMeetings)} />
            <Route
              path="my-contact-info"
              component={RequireAuth(MyContactInfo)}
            />
            <Route path="my-resources" component={RequireAuth(MyResources)} />
            <Route path="elections" component={RequireAuth(Elections)} />
            <Route
              path="elections/:electionId"
              component={RequireAuth(ElectionDetail)}
            />
            <Route
              path="my-elections/view-vote"
              component={RequireAuth(ViewVote)}
            />
            <Route
              path="elections/:electionId/edit"
              component={RequireAuth(ElectionEdit)}
            />
            <Route
              path="elections/:electionId/results"
              component={RequireAuth(RankedChoiceVisualization)}
            />
            <Route
              path="elections/:electionId/results/fullscreen"
              component={RequireAuth(RankedChoiceVisualization)}
            />
            <Route
              path="admin/elections/:electionId/vote"
              component={RequireAuth(EnterVote)}
            />
            <Route
              path="elections/:electionId/print"
              component={RequireAuth(PrintBallots)}
            />
            <Route
              path="elections/:electionId/signin"
              component={RequireAuth(SignInKiosk)}
            />
            <Route
              path="admin/elections/:electionId/vote"
              component={RequireAuth(EnterVote)}
            />
            <Route
              path="elections/:electionId/print"
              component={RequireAuth(PrintBallots)}
            />
            <Route
              path="elections/:electionId/signin"
              component={RequireAuth(SignInKiosk)}
            />
            <Route path="vote/:electionId" component={RequireAuth(Vote)} />
            <Route path="email" component={RequireAuth(EmailPage)} />
            <Route path="email/rules" component={RequireAuth(EmailRules)} />
            <Route
              path="email/newTemplate"
              component={RequireAuth(CreateEmailTemplate)}
            />
            <Route
              path="email/template/:templateId"
              component={RequireAuth(EmailTemplateDetails)}
            />
          </Route>
          <Route path="*" component={NotFound} />
        </Route>
      </Router>
    )
  }
}

const mapStateToProps = state => state
const mapDispatchToProps = dispatch => ({
  signOutUser: () => dispatch(signoutUser())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routes)
