const webpack = require('webpack')
const DashboardPlugin = require('webpack-dashboard/plugin')
const path = require('path')

const dotenvConfig = process.env.DOTENV_CONFIG
const dotenvOpts = dotenvConfig && { path: dotEnvConfig }

require('dotenv').config(dotenvOpts)

const DEV_SERVER_PORT = process.env.DEV_SERVER_PORT || 3000
const DEV_SERVER_HOST = process.env.DEV_SERVER_HOST || 'localhost'

module.exports = {
  mode: 'development',
  devtool: '#cheap-module-source-map',
  entry: [
    'react-hot-loader/patch',
    `webpack-dev-server/client?http://localhost:${DEV_SERVER_PORT}`,
    'webpack/hot/only-dev-server',
    'babel-polyfill',
    './src/js/index.jsx'
  ],
  devServer: { host: DEV_SERVER_HOST, port: DEV_SERVER_PORT },
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: './bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.js[x]?$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader?limit=8192'
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new DashboardPlugin(),
    new webpack.EnvironmentPlugin([
      'NODE_ENV',
      'USE_AUTH',
      'MEMBERSHIP_API_URL',
      'AUTH0_CLIENT_ID',
      'AUTH0_DOMAIN',
      'DEV_SERVER_PORT'
    ]),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    })
  ],
  resolve: {
    extensions: ['.js', '.jsx']
  }
}
