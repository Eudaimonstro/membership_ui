import React from "react";
import configureStore from "../../../src/js/redux/store/configureStore";
import { serviceLocator } from "../../../src/js/util/serviceLocator";
import Committees from "../../../src/js/components/committee/Committees";

import { expect } from "chai";
import { shallow, configure } from "enzyme";
import { fromJS } from "immutable";
import Adapter from "enzyme-adapter-react-15";

configure({ adapter: new Adapter() });

const store = configureStore({
  member: fromJS({
    isAdmin: true
  })
});
serviceLocator.store = store;

describe("<Committees/>", () => {
  xit("works", done => {
    const wrapper = shallow(<Committees store={store} />).dive();
    expect(wrapper.length > 0);
    done();
  });
});
