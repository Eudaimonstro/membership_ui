const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const path = require('path')

const config = require('./webpack.config')

new WebpackDevServer(webpack(config), {
  contentBase: path.resolve(__dirname, 'src'),
  disableHostCheck: true,
  historyApiFallback: true,
  hot: true,
  progress: true,
  publicPath: config.output.publicPath,
  stats: { colors: true }
}).listen(config.devServer.port, config.devServer.host, (err, result) => {
  if (err) return console.log(err)

  console.log(
    `Listening at http://${config.devServer.host}:${config.devServer.port}`
  )
})
